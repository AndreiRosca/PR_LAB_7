package md.utm.pr.labs.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import md.utm.sm.labs.net.task.Broadcaster;
import md.utm.sm.labs.net.task.StreamingGroup;

public class PortManagementServer implements Runnable {

  private static final int PORT = 6000;
  private static final int MAX_DATAGRAM_SIZE = 10_000;
  private static final int STARTING_BROADCAST_PORT = 11_000;
  private static final int STARTING_MULTICAST_PORT = 22_000;
  private static final Pattern HEADER_PATTERN = Pattern.compile("(.+): (.+)");
  private static final String BROADCAST_ADDRESS = "192.168.1.255";

  private volatile boolean stopRequested = false;
  private Map<Integer, String> broadcasters = new ConcurrentHashMap<>();
  private Map<Integer, String> groups = new ConcurrentHashMap<>();

  public static void main(String[] args) throws InterruptedException {
    PortManagementServer server = new PortManagementServer();
    server.start();
    TimeUnit.MINUTES.sleep(5);
    server.stop();
  }

  public void start() {
    Thread t = new Thread(this);
    t.start();
  }

  public void stop() {
    stopRequested = true;
  }

  @Override
  public void run() {
    DatagramSocket socket = null;
    try {
      socket = new DatagramSocket(PORT);
      while (!stopRequested) {
        byte[] buffer = new byte[MAX_DATAGRAM_SIZE];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        System.out.println("Received request from: " + packet.getAddress() + ":" + packet.getPort());
        handleRequest(packet, socket);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      if (socket != null)
        socket.close();
    }
  }

  private void handleRequest(DatagramPacket packet, DatagramSocket socket) {
    String request = new String(packet.getData(), 0, packet.getLength());
    try (BufferedReader reader = new BufferedReader(new StringReader(request))) {
      String line = reader.readLine();
      Map<String, String> headers = readHeaders(reader);
      Command command = getCommand(line, packet);
      command.execute(headers, socket);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Command getCommand(String headerLine, DatagramPacket packet) {
    if (headerLine.startsWith("GET PORT") && !headerLine.contains("RESPONSE"))
      return new GetFreePortCommand(packet);
    if (headerLine.startsWith("GET BROADCASTERS") && !headerLine.contains("RESPONSE"))
      return new GetBroadcastersCommand(packet);
    if (headerLine.startsWith("CREATE GROUP") && !headerLine.contains("RESPONSE"))
      return new CreateGroupCommand(packet);
    if (headerLine.startsWith("LIST GROUPS") && !headerLine.contains("RESPONSE"))
      return new ListGroupsCommand(packet);
    return Command.NULL;
  }

  private class GetFreePortCommand implements Command {

    private final DatagramPacket packet;

    public GetFreePortCommand(DatagramPacket packet) {
      this.packet = packet;
    }

    @Override
    public void execute(Map<String, String> headers, DatagramSocket socket) {
      int port = getNextAvailablePort();
      broadcasters.put(port, headers.get("Broadcaster-Name"));
      sendResponse(port, socket);
    }

    private void sendResponse(int port, DatagramSocket socket) {
      try {
        String response = buildPayload(port);
        byte[] data = response.getBytes();
        DatagramPacket responsePacket = new DatagramPacket(data, data.length);
        responsePacket.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
        responsePacket.setPort(packet.getPort());// 1!!1
        socket.send(responsePacket);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    private String buildPayload(int port) {
      return new StringBuilder().append("GET PORT RESPONSE")
          .append("\r\n")
          .append("Available-Port: ")
          .append(port)
          .append("\r\n")
          .append("\r\n")
          .toString();
    }

  }

  private int getNextAvailablePort() {
    Optional<Integer> maxPort = broadcasters.keySet()
        .stream()
        .sorted(Integer::compare)
        .max(Integer::compare);
    return maxPort.orElseGet(() -> STARTING_BROADCAST_PORT) + 1;
  }

  public Map<String, String> readHeaders(BufferedReader reader) throws IOException {
    Map<String, String> headers = new HashMap<>();
    String header = "";
    do {
      header = reader.readLine();
      if (header == null)
        break;
      Matcher m = HEADER_PATTERN.matcher(header);
      if (m.find())
        headers.put(m.group(1), m.group(2));
    } while (!"".equals(header));
    return headers;
  }

  private class GetBroadcastersCommand implements Command {

    private final DatagramPacket packet;

    public GetBroadcastersCommand(DatagramPacket packet) {
      this.packet = packet;
    }

    @Override
    public void execute(Map<String, String> headers, DatagramSocket socket) {
      List<Broadcaster> list = broadcasters.entrySet()
          .stream()
          .map(e -> new Broadcaster(e.getKey(), e.getValue()))
          .collect(Collectors.toList());
      String responseBody = buildResponseBody(list);
      sendResponse(responseBody, socket);
    }

    private void sendResponse(String responseBody, DatagramSocket socket) {
      try {
        String response = buildPayload(responseBody);
        byte[] data = response.getBytes();
        DatagramPacket responsePacket = new DatagramPacket(data, data.length);
        responsePacket.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
        responsePacket.setPort(packet.getPort());// !!!
        socket.send(responsePacket);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    private String buildPayload(String body) {
      return new StringBuilder().append("GET BROADCASTERS RESPONSE")
          .append("\r\n")
          .append("Content-Transfer-Encoding: text/plain")
          .append("\r\n")
          .append("\r\n")
          .append(body)
          .append("\r\n")
          .toString();
    }

    private String buildResponseBody(List<Broadcaster> list) {
      StringBuilder payload = new StringBuilder();
      for (Broadcaster b : list)
        payload.append(b.getPort())
            .append(", ")
            .append(b.getName())
            .append("\r\n");
      payload.append("\r\n");
      return payload.toString();
    }
  }

  private class CreateGroupCommand implements Command {

    private final DatagramPacket packet;

    public CreateGroupCommand(DatagramPacket packet) {
      this.packet = packet;
    }

    @Override
    public void execute(Map<String, String> headers, DatagramSocket socket) {
      int port = getNextMulticastAvailablePort();
      groups.put(port, headers.get("Target-Group-Name"));
      sendResponse(port, headers, socket);
    }

    private void sendResponse(int port, Map<String, String> headers, DatagramSocket socket) {
      String payload = buildPayload(port, headers);
      byte[] data = payload.getBytes();
      try {
        DatagramPacket responsePacket = new DatagramPacket(data, data.length);
        responsePacket.setPort(packet.getPort());
        responsePacket.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
        socket.send(responsePacket);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    private String buildPayload(int port, Map<String, String> headers) {
      return new StringBuilder().append("CREATED GROUP")
          .append("\r\n")
          .append("Group-Port: ")
          .append(port)
          .append("\r\n")
          .append("Target-Group-Name: ")
          .append(headers.get("Target-Group-Name"))
          .append("\r\n")
          .append("\r\n")
          .toString();
    }

    private int getNextMulticastAvailablePort() {
      Optional<Integer> maxPort = groups.keySet()
          .stream()
          .sorted(Integer::compare)
          .max(Integer::compare);
      return maxPort.orElseGet(() -> STARTING_MULTICAST_PORT) + 1;
    }
  }

  private class ListGroupsCommand implements Command {

    private final DatagramPacket packet;

    public ListGroupsCommand(DatagramPacket packet) {
      this.packet = packet;
    }

    @Override
    public void execute(Map<String, String> headers, DatagramSocket socket) {
      List<StreamingGroup> streamingGroups = groups.entrySet()
          .stream()
          .map(e -> new StreamingGroup(e.getKey(), e.getValue()))
          .collect(Collectors.toList());
      String body = buildResponseBody(streamingGroups);
      sendResponse(body, socket);
    }

    private void sendResponse(String body, DatagramSocket socket) {
      try {
        String payload = buildPayload(body);
        byte[] data = payload.getBytes();
        DatagramPacket responsePacket = new DatagramPacket(data, data.length);
        responsePacket.setPort(packet.getPort());
        responsePacket.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
        socket.send(responsePacket);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    
    private String buildPayload(String body) {
      return new StringBuilder().append("GROUP LIST")
          .append("\r\n")
          .append("Content-Transfer-Encoding: text/plain")
          .append("\r\n")
          .append("\r\n")
          .append(body)
          .append("\r\n")
          .toString();
    }

    private String buildResponseBody(List<StreamingGroup> streamingGroups) {
      StringBuilder body = new StringBuilder();
      for (StreamingGroup g : streamingGroups)
        body.append(g.getPort())
            .append(", ")
            .append(g.getName())
            .append("\r\n");
      body.append("\r\n");
      return body.toString();
    }
  }
}
