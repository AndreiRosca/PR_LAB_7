package md.utm.pr.labs.server;

import java.net.DatagramSocket;
import java.util.Map;

public interface Command {
  void execute(Map<String, String> headers, DatagramSocket socket);

  Command NULL = new Command() {

    @Override
    public void execute(Map<String, String> headers, DatagramSocket socket) {
      System.out.println("NULL Command fallback");
    }
  };
}
