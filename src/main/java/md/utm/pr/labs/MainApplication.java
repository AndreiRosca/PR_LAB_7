package md.utm.pr.labs;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import md.utm.pr.labs.controller.StartController;

public class MainApplication extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setController(new StartController());
		loader.load(getClass().getResourceAsStream("/main.fxml"));
		Parent parent = loader.getRoot();
		primaryStage.setScene(new Scene(parent));
		primaryStage.setTitle("Video streamer v1.0");
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
