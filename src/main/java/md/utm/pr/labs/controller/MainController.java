package md.utm.pr.labs.controller;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import md.utm.sm.labs.event.handler.impl.WebcamRecorderWorker;
import md.utm.sm.labs.mediator.FilterChangeListener;
import md.utm.sm.labs.mediator.VideoMediator;
import md.utm.sm.labs.webcam.FrameProcessor;
import md.utm.sm.labs.webcam.WebcamRecorder;
import md.utm.sm.labs.webcam.filters.CristalizeWebcamFilter;
import md.utm.sm.labs.webcam.filters.DitherWebcamFilter;
import md.utm.sm.labs.webcam.filters.ExposureWebcamFilter;
import md.utm.sm.labs.webcam.filters.FbmWebcamFilter;
import md.utm.sm.labs.webcam.filters.GammaWebcamFilter;
import md.utm.sm.labs.webcam.filters.GaussianWebcamFilter;
import md.utm.sm.labs.webcam.filters.GlowWebcamFilter;
import md.utm.sm.labs.webcam.filters.GrayscaleWebcamFilter;
import md.utm.sm.labs.webcam.filters.InvertWebcamFilter;
import md.utm.sm.labs.webcam.filters.KaleidoscopeWebcamFilter;
import md.utm.sm.labs.webcam.filters.LightWebcamFilter;
import md.utm.sm.labs.webcam.filters.NoiseWebcamFilter;
import md.utm.sm.labs.webcam.filters.NoneWebcamFilter;
import md.utm.sm.labs.webcam.filters.SharperWebcamFilter;
import md.utm.sm.labs.webcam.filters.SolarizeWebcamFilter;
import md.utm.sm.labs.webcam.filters.SphereWebcamFilter;
import md.utm.sm.labs.webcam.filters.ThresholdWebcamFilter;
import md.utm.sm.labs.webcam.filters.WaterWebcamFilter;

public class MainController implements Initializable, VideoMediator {
  private List<FilterChangeListener> filterListeners = new CopyOnWriteArrayList<>();

  @FXML
  private AnchorPane anchorPane;

  @FXML
  private MenuBar menuBar;

  @FXML
  private ImageView webcamImageView;

  private ObjectProperty<Image> imageProperty = new SimpleObjectProperty<Image>();
  private WebcamRecorder recorder = WebcamRecorder.NULL_RECORDER;

  @FXML
  private RadioButton noneFilterRadioButton;

  @FXML
  private RadioButton grayscaleFilterRadioButton;

  @FXML
  private RadioButton gammaFilterRadioButton;

  @FXML
  private RadioButton noiseFilterRadioButton;

  @FXML
  private RadioButton lightFilterRadioButton;

  @FXML
  private RadioButton gaussianFilterRadioButton;

  @FXML
  private RadioButton kaleidoscopeFilterRadioButton;

  @FXML
  private RadioButton sharpenFilterRadioButton;

  @FXML
  private RadioButton solarizeFilterRadioButton;

  @FXML
  private RadioButton thresholdFilterRadioButton;

  @FXML
  private RadioButton waterFilterRadioButton;

  @FXML
  private RadioButton sphereFilterRadioButton;

  @FXML
  private RadioButton invertFilterRadioButton;

  @FXML
  private RadioButton glowFilterRadioButton;

  @FXML
  private RadioButton exposureFilterRadioButton;

  @FXML
  private RadioButton fbmFilterRadioButton;

  @FXML
  private RadioButton ditherFilterRadioButton;

  @FXML
  private RadioButton cristalizeFilterRadioButton;

  @FXML
  private Label motionStatusLabel;

  private ToggleGroup group;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    webcamImageView.imageProperty()
        .bind(imageProperty);
    setUpToggleGroup();
  }
  
  public void start(Consumer<Void> consumer) {
    consumer.accept(null);
  }

  private void setUpToggleGroup() {
    group = new ToggleGroup();
    grayscaleFilterRadioButton.setUserData(new GrayscaleWebcamFilter());
    grayscaleFilterRadioButton.setToggleGroup(group);
    gammaFilterRadioButton.setUserData(new GammaWebcamFilter());
    gammaFilterRadioButton.setToggleGroup(group);
    noneFilterRadioButton.setUserData(new NoneWebcamFilter());
    noneFilterRadioButton.setToggleGroup(group);
    noiseFilterRadioButton.setUserData(new NoiseWebcamFilter());
    noiseFilterRadioButton.setToggleGroup(group);
    lightFilterRadioButton.setUserData(new LightWebcamFilter());
    lightFilterRadioButton.setToggleGroup(group);
    gaussianFilterRadioButton.setUserData(new GaussianWebcamFilter());
    gaussianFilterRadioButton.setToggleGroup(group);
    kaleidoscopeFilterRadioButton.setUserData(new KaleidoscopeWebcamFilter());
    kaleidoscopeFilterRadioButton.setToggleGroup(group);
    sharpenFilterRadioButton.setUserData(new SharperWebcamFilter());
    sharpenFilterRadioButton.setToggleGroup(group);
    solarizeFilterRadioButton.setUserData(new SolarizeWebcamFilter());
    solarizeFilterRadioButton.setToggleGroup(group);
    thresholdFilterRadioButton.setUserData(new ThresholdWebcamFilter());
    thresholdFilterRadioButton.setToggleGroup(group);
    waterFilterRadioButton.setUserData(new WaterWebcamFilter());
    waterFilterRadioButton.setToggleGroup(group);
    sphereFilterRadioButton.setUserData(new SphereWebcamFilter());
    sphereFilterRadioButton.setToggleGroup(group);
    invertFilterRadioButton.setUserData(new InvertWebcamFilter());
    invertFilterRadioButton.setToggleGroup(group);
    glowFilterRadioButton.setUserData(new GlowWebcamFilter());
    glowFilterRadioButton.setToggleGroup(group);
    exposureFilterRadioButton.setUserData(new ExposureWebcamFilter());
    exposureFilterRadioButton.setToggleGroup(group);
    fbmFilterRadioButton.setUserData(new FbmWebcamFilter());
    fbmFilterRadioButton.setToggleGroup(group);
    ditherFilterRadioButton.setUserData(new DitherWebcamFilter());
    ditherFilterRadioButton.setToggleGroup(group);
    cristalizeFilterRadioButton.setUserData(new CristalizeWebcamFilter());
    cristalizeFilterRadioButton.setToggleGroup(group);
    group.selectedToggleProperty()
        .addListener(new WebcamFilterChangedListener());
  }

  @Override
  public void setWebcamFrame(BufferedImage frame) {
    Platform.runLater(() -> {
      final Image image = SwingFXUtils.toFXImage(frame, null);
      imageProperty.set(image);
    });
  }

  @Override
  public void clearWebcamImageView() {
    imageProperty.set(null);
  }

  @Override
  public void setWebcamRecorder(WebcamRecorder recorder) {
    this.recorder = recorder;
  }

  @Override
  public WebcamRecorder getWebcamRecorder() {
    return recorder;
  }

  private class WebcamFilterChangedListener implements ChangeListener<Toggle> {

    @Override
    public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
      if (group.getSelectedToggle() != null) {
        FrameProcessor processor = (FrameProcessor) group.getSelectedToggle()
            .getUserData();
        filterListeners.forEach(l -> l.onFilterChange(processor));
        recorder.setFrameProcessor(processor);
      }
    }
  }

  @Override
  public void onMotionDetected() {
    Platform.runLater(() -> {
      motionStatusLabel.setText("Motion detection status: Detected!");
    });
  }

  @Override
  public void onMotionNotDetected() {
    Platform.runLater(() -> {
      motionStatusLabel.setText("Motion detection status: not detected.");
    });
  }

  @Override
  public void enableMotionDetectionStatus() {
    motionStatusLabel.setVisible(true);
  }

  @Override
  public void registerFilterChangeListener(FilterChangeListener listener) {
    filterListeners.add(listener);
  }
}
