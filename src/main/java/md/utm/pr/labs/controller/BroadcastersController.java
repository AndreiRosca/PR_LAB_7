package md.utm.pr.labs.controller;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import md.utm.sm.labs.net.task.Broadcaster;
import md.utm.sm.labs.net.task.BroadcastersFetcher;

public class BroadcastersController implements Initializable {
  private static final Map<Integer, PropertyValueFactory<Broadcaster, String>> columnFactories = new HashMap<>();

  static {
    columnFactories.put(0, new PropertyValueFactory<>("name"));
    columnFactories.put(1, new PropertyValueFactory<>("port"));
  }

  @FXML
  private TableView<Broadcaster> broadcastersTableView;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    int i = 0;
    for (TableColumn column : broadcastersTableView.getColumns()) {
      column.setCellValueFactory(columnFactories.get(i++));
    }
    
    broadcastersTableView.setRowFactory( tv -> {
      TableRow<Broadcaster> row = new TableRow<>();
      row.setOnMouseClicked(event -> {
          if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
            Broadcaster rowData = row.getItem();
             startWatchingBroadcaster(rowData);
          }
      });
      return row ;
  });

    BroadcastersFetcher fetcher = new BroadcastersFetcher();
    fetcher.getBroadcasters()
        .thenAccept(l -> {
          Platform.runLater(() -> {
            l.forEach(b -> {
              broadcastersTableView.getItems().add(b);
              broadcastersTableView.scrollTo(b);
            });            
          });
        });
  }

  private void startWatchingBroadcaster(Broadcaster broadcaster) {
   try {
     FXMLLoader loader = new FXMLLoader();
     loader.setController(new WatchBroadcastController(broadcaster));
     loader.load(getClass().getResourceAsStream("/watchBroadcast.fxml"));
     Parent parent = loader.getRoot();
     Stage primaryStage = new Stage();
     primaryStage.setScene(new Scene(parent));
     primaryStage.setTitle("Video streamer v1.0");
     primaryStage.show();
     Stage stage = (Stage) broadcastersTableView.getScene().getWindow();
     stage.close();
   } catch (Exception e) {
     throw new RuntimeException(e);
   }
  }
}
