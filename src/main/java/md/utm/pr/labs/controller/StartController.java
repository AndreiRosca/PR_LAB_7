package md.utm.pr.labs.controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import md.utm.sm.labs.event.handler.impl.WebcamRecorderWorker;
import md.utm.sm.labs.net.task.CreateGroupTask;
import md.utm.sm.labs.webcam.impl.WebcamRecorderMulticastWorker;

public class StartController implements Initializable {

  @FXML
  private Button startBroadcasting;

  @FXML
  public void onCreateStreamingGroupPressed(ActionEvent event) {
    TextInputDialog dialog = new TextInputDialog("");
    dialog.setTitle("Create new group");
    dialog.setHeaderText("Enter the streaming group name");
    dialog.setContentText("Group name name:");

    // Traditional way to get the response value.
    Optional<String> result = dialog.showAndWait();
    if (result.isPresent()) {
      String groupName = result.get();

      new CreateGroupTask(groupName).start()
          .thenAccept(g -> {
            Platform.runLater(() -> {
              try {
                System.out.println(g);
                FXMLLoader loader = new FXMLLoader();
                MainController c = new MainController();
                c.start(a -> new WebcamRecorderMulticastWorker(g).startRecording(c));
                loader.setController(c);
                loader.load(getClass().getResourceAsStream("/layout.fxml"));
                Parent parent = loader.getRoot();
                Stage primaryStage = new Stage();
                primaryStage.setScene(new Scene(parent));
                primaryStage.setTitle("Video streamer v1.0");
                primaryStage.show();
                Stage stage = (Stage) startBroadcasting.getScene()
                    .getWindow();
                stage.close();
              } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
              }

            });
          });
    }
  }

  @FXML
  public void onViewStreamingGroupsPressed(ActionEvent event) {
    try {
      FXMLLoader loader = new FXMLLoader();
      loader.setController(new GroupsController());
      loader.load(getClass().getResourceAsStream("/groups.fxml"));
      Parent parent = loader.getRoot();
      Stage primaryStage = new Stage();
      primaryStage.setScene(new Scene(parent));
      primaryStage.setTitle("Video streamer v1.0");
      primaryStage.show();
      Stage stage = (Stage) startBroadcasting.getScene()
          .getWindow();
      stage.close();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @FXML
  public void onStartBroadcastingPressed(ActionEvent event) {
    try {
      FXMLLoader loader = new FXMLLoader();
      MainController c = new MainController();
      c.start(a -> new WebcamRecorderWorker().startRecording(c));
      loader.setController(c);
      loader.load(getClass().getResourceAsStream("/layout.fxml"));
      Parent parent = loader.getRoot();
      Stage primaryStage = new Stage();
      primaryStage.setScene(new Scene(parent));
      primaryStage.setTitle("Video streamer v1.0");
      primaryStage.show();
      Stage stage = (Stage) startBroadcasting.getScene()
          .getWindow();
      stage.close();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @FXML
  public void onViewBroadcastersList(ActionEvent event) {
    try {
      FXMLLoader loader = new FXMLLoader();
      loader.setController(new BroadcastersController());
      loader.load(getClass().getResourceAsStream("/broadcasters.fxml"));
      Parent parent = loader.getRoot();
      Stage primaryStage = new Stage();
      primaryStage.setScene(new Scene(parent));
      primaryStage.setTitle("Video streamer v1.0");
      primaryStage.show();
      Stage stage = (Stage) startBroadcasting.getScene()
          .getWindow();
      stage.close();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // TODO Auto-generated method stub

  }
}
