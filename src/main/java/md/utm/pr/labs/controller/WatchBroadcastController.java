package md.utm.pr.labs.controller;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import md.utm.sm.labs.mediator.FilterChangeListener;
import md.utm.sm.labs.mediator.VideoMediator;
import md.utm.sm.labs.net.task.Broadcaster;
import md.utm.sm.labs.net.task.FrameListener;
import md.utm.sm.labs.net.task.FrameSubscriber;
import md.utm.sm.labs.net.task.StreamingGroup;
import md.utm.sm.labs.webcam.WebcamRecorder;

public class WatchBroadcastController implements Initializable, VideoMediator, FrameListener {
  @FXML
  private AnchorPane anchorPane;

  @FXML
  private ImageView webcamImageView;

  private ObjectProperty<Image> imageProperty = new SimpleObjectProperty<Image>();
  private WebcamRecorder recorder = WebcamRecorder.NULL_RECORDER;

  private Broadcaster broadcaster;

  private StreamingGroup group;

  public WatchBroadcastController(Broadcaster broadcaster) {
    this.broadcaster = broadcaster;
  }
  
  public WatchBroadcastController(StreamingGroup group) {
    this.group = group;
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    webcamImageView.imageProperty()
        .bind(imageProperty);
    
    FrameSubscriber subscriber = new FrameSubscriber(broadcaster != null ? broadcaster.getPort() : group.getPort());
    subscriber.setMulticast(true);
    subscriber.registerListener(this);
    subscriber.start();
  }

  @Override
  public void setWebcamFrame(BufferedImage frame) {
    Platform.runLater(() -> {
      final Image image = SwingFXUtils.toFXImage(frame, null);
      imageProperty.set(image);
    });
  }

  @Override
  public void clearWebcamImageView() {
    imageProperty.set(null);
  }

  @Override
  public void setWebcamRecorder(WebcamRecorder recorder) {
    this.recorder = recorder;
  }

  @Override
  public WebcamRecorder getWebcamRecorder() {
    return recorder;
  }

  @Override
  public void onMotionDetected() {
  }

  @Override
  public void onMotionNotDetected() {
  }

  @Override
  public void enableMotionDetectionStatus() {
  }

  @Override
  public void registerFilterChangeListener(FilterChangeListener listener) {
  }

  @Override
  public void consumeFrame(BufferedImage frame) {
    Platform.runLater(() -> setWebcamFrame(frame));
  }
}
