package md.utm.pr.labs.event.handler.factory;

import md.utm.sm.labs.event.handler.impl.*;

public class MenuEventHandlerFactory {

	public static void makeHandlers() {
		new QuitMenuEventHandler();
		new AboutMenuEventHandler();
		new StartWebcamRecordingEventHandler();
		new StopWebcamRecordingEventHandler();
		new FaceRecognitionEventHandler();
		new StartWatchingBroadcastEventHandler();
		new StopWatchingBroadcastEventHandler();
	}
}
