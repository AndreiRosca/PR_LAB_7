package md.utm.pr.labs.event.handler;

import md.utm.pr.labs.controller.MainController;
import md.utm.sm.labs.mediator.VideoMediator;

public interface MenuEventHandler {
	void handle();
	
	public default VideoMediator getMediator() {
		return null;
	}
}
