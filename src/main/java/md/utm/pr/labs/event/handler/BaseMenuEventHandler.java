package md.utm.pr.labs.event.handler;

public abstract class BaseMenuEventHandler implements MenuEventHandler {
	
	public BaseMenuEventHandler(String eventId) {
		MenuEventHandlerRegistry registry = MenuEventHandlerRegistry.getInstance();
		registry.registerHandler(eventId, this);
	}
}
