package md.utm.sm.labs.net.task;

public class Broadcaster {

  private int port;
  private String name;

  public Broadcaster(int port, String name) {
    this.port = port;
    this.name = name;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format("Broadcaster{port=%s, name=%s}", port, name);
  }

}
