package md.utm.sm.labs.net.task;

import java.io.BufferedReader;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class CreateGroupTask {
  
  private static final int MAX_PACKET_SIZE = 40_000;
  private static final String BROADCAST_ADDRESS = "192.168.1.255";
  private static final int PORT = 6000;

  private String groupName;

  public CreateGroupTask(String groupName) {
   this.groupName = groupName;
  }

  public CompletableFuture<StreamingGroup> start() {
    CompletableFuture<StreamingGroup> future = new CompletableFuture<>();
    new SendCreateGroupTask(future).start();
    return future;
  }

  private class SendCreateGroupTask extends Thread {
    
    private CompletableFuture<StreamingGroup> future;

    public SendCreateGroupTask(CompletableFuture<StreamingGroup> future) {
     this.future = future;
    }

    public void run() {
      DatagramSocket socket = null;
      try {
        socket = new DatagramSocket();
        DatagramPacket requestPacket = sendCreateGroupRequest(socket);
        StreamingGroup group = receiveCreateGroupResponse(socket, requestPacket);
        future.complete(group);
      } catch (Exception e) {
        throw new RuntimeException(e);
      } finally {
        if (socket != null)
          socket.close();
      }
    }

    private StreamingGroup receiveCreateGroupResponse(DatagramSocket socket, DatagramPacket requestPacket) throws Exception {
      byte[] buffer = new byte[MAX_PACKET_SIZE];
      DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
      packet.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
      packet.setPort(requestPacket.getPort());
      socket.receive(packet);
      return readResponseBody(packet);
    }

    private StreamingGroup readResponseBody(DatagramPacket packet) {
      try (BufferedReader reader = new BufferedReader(new StringReader(new String(packet.getData())))) {
        if (reader.readLine().startsWith("CREATED GROUP")) {
          Map<String, String> headers = RequestUtil.readHeaders(reader);
          return new StreamingGroup(Integer.valueOf(headers.get("Group-Port")), groupName);
        }
        return null;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    private DatagramPacket sendCreateGroupRequest(DatagramSocket socket) throws Exception {
      String request = "CREATE GROUP\r\nTarget-Group-Name: " + groupName + "\r\n\r\n";
      byte[] data = request.getBytes();
      DatagramPacket packet = new DatagramPacket(data, data.length);
      packet.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
      packet.setPort(PORT);
      socket.send(packet);
      return packet;
    }
  }
}
