package md.utm.sm.labs.net.task;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.imageio.ImageIO;

public class FramePublisher implements Runnable {
  
  private static final String BROADCAST_ADDRESS = "192.168.1.255";

  private final BufferedImage frame;
  private final int port;

  private String address;

  public FramePublisher(BufferedImage frame, int port, String ip) {
    this.frame = frame;
    this.port = port;
    this.address = ip;
  }
  
  public FramePublisher(BufferedImage frame, int port) {
    this.frame = frame;
    this.port = port;
  }

  @Override
  public void run() {
    byte[] header = "CONSUME FRAME\r\n\r\n".getBytes();
    byte[] image = getFrameAsByteArray(header);
    DatagramPacket packet = new DatagramPacket(image, image.length);
    broadcastPacket(packet);
  }

  private void broadcastPacket(DatagramPacket packet) {
    DatagramSocket socket = null;
    try {
      socket = new DatagramSocket();
      packet.setAddress(InetAddress.getByName(address != null ? address : BROADCAST_ADDRESS));
      packet.setPort(port);
      socket.send(packet);
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {      
      socket.close();
    }
  }

  private byte[] getFrameAsByteArray(byte[] header) {
    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      out.write(header);
      ImageIO.write(frame, "jpg", out);
      out.flush();
      return out.toByteArray();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
