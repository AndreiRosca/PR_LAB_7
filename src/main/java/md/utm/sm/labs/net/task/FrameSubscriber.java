package md.utm.sm.labs.net.task;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.imageio.ImageIO;

public class FrameSubscriber implements Runnable {

  private static final int MAX_PACKET_SIZE = 65_000;

  private volatile boolean stopRequested = false;
  private final Set<FrameListener> listeners = new CopyOnWriteArraySet<>();
  private final int port;

  private boolean isMulticast;

  public FrameSubscriber(int port) {
    this.port = port;
  }

  public void registerListener(FrameListener listener) {
    listeners.add(listener);
  }

  public void start() {
    Thread t = new Thread(this);
    t.setDaemon(true);
    t.start();
  }
  
  public void stop() {
    stopRequested = true;
  }

  @Override
  public void run() {
    DatagramSocket socket = null;
    try {
      if (isMulticast) {
        socket = new MulticastSocket(port);
        ((MulticastSocket) socket).joinGroup(InetAddress.getByName("230.1.1.1"));
      } else
        socket = new DatagramSocket(port);
      while (!stopRequested) {
        byte[] buffer = new byte[MAX_PACKET_SIZE];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        byte[] data = packet.getData();
        publishReceivedFrame(data);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      socket.close();
    }
  }

  private void publishReceivedFrame(byte[] data) {
    if (messageIsNewFrame(data)) {
      BufferedImage image = toBufferedImage(data);
      listeners.forEach(l -> l.consumeFrame(image));      
    }
  }

  private boolean messageIsNewFrame(byte[] data) {
    int bodyIndex = getBodyIndex(data, 0);
    String requestLine = new String(data, 0, bodyIndex);
    return requestLine.startsWith("CONSUME FRAME");
  }

  private BufferedImage toBufferedImage(byte[] data) {
    int bodyIndex = getBodyIndex(data, 0);
    data = Arrays.copyOfRange(data, bodyIndex, data.length);
    ByteArrayInputStream in = new ByteArrayInputStream(data);
    try {
      return ImageIO.read(in);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private int getBodyIndex(byte[] httpResponse, int startingIndex) {
    for (int i = startingIndex; i < httpResponse.length; ++i)
      if (isNewLine(httpResponse, i) && isNewLine(httpResponse, i + 2))
        return i + 4;
    return -1;
  }

  private boolean isNewLine(byte[] httpResponse, int i) {
    return httpResponse[i] == 0xD && httpResponse[i + 1] == 0xA;
  }

  public void setMulticast(boolean isMulticast) {
    this.isMulticast = isMulticast;
  }
}
