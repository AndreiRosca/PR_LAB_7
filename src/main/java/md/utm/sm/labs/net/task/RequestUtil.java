package md.utm.sm.labs.net.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequestUtil {
  private static final Pattern HEADER_PATTERN = Pattern.compile("(.+): (.+)");

  public static Map<String, String> readHeaders(BufferedReader reader) throws IOException {
    Map<String, String> headers = new HashMap<>();
    String header = "";
    do {
      header = reader.readLine();
      if (header == null)
        break;
      Matcher m = HEADER_PATTERN.matcher(header);
      if (m.find())
        headers.put(m.group(1), m.group(2));
    } while (!"".equals(header));
    return headers;
  }

  public static String readBody(BufferedReader reader) {
    StringBuilder payload = new StringBuilder();
    String line;
    try {
      while ((line = reader.readLine()) != null) {
        payload.append(line);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return payload.toString();
  }
}
