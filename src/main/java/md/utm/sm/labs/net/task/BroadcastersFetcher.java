package md.utm.sm.labs.net.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BroadcastersFetcher {
  
  private static final int MAX_PACKET_SIZE = 40_000;
  private static final String BROADCAST_ADDRESS = "192.168.1.255";
  private static final int PORT = 6000;

  public CompletableFuture<List<Broadcaster>> getBroadcasters() {
    CompletableFuture<List<Broadcaster>> future = new CompletableFuture<>();
    GetBroadcastersTask task = new GetBroadcastersTask(future);
    task.start();
    return future;
  }

  private static class GetBroadcastersTask extends Thread {
    private final CompletableFuture<List<Broadcaster>> future;

    public GetBroadcastersTask(CompletableFuture<List<Broadcaster>> future) {
      this.future = future;
    }

    @Override
    public void run() {
      DatagramSocket socket = null;
      try {
        socket = new DatagramSocket();
        DatagramPacket requestPacket = sendGetBroadcastersRequest(socket);
        List<Broadcaster> broadcasters = receiveGetBroadcastersResponse(socket, requestPacket);
        future.complete(broadcasters);
      } catch (Exception e) {
        throw new RuntimeException(e);
      } finally {
        if (socket != null)
          socket.close();
      }
    }

    private List<Broadcaster> receiveGetBroadcastersResponse(DatagramSocket socket, DatagramPacket requestPacket) throws IOException {
      byte[] buffer = new byte[MAX_PACKET_SIZE];
      DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
      packet.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
      packet.setPort(requestPacket.getPort());
      socket.receive(packet);
      return readResponseBody(packet);
    }

    private List<Broadcaster> readResponseBody(DatagramPacket packet) {
      try (BufferedReader reader = new BufferedReader(new StringReader(new String(packet.getData())))) {
        if (reader.readLine().startsWith("GET BROADCASTERS RESPONSE")) {
          Map<String, String> headers = RequestUtil.readHeaders(reader);
          String body = RequestUtil.readBody(reader);
          List<Broadcaster> broadcasters = readResponsePayload(body);
          return broadcasters;
        }
        return new ArrayList<>();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

    private List<Broadcaster> readResponsePayload(String body) {
      List<Broadcaster> broadcasters = new ArrayList<>();
      Pattern pattern = Pattern.compile("(\\d+), (.+)");
      Matcher matcher = pattern.matcher(body);
      while (matcher.find()) {
        broadcasters.add(new Broadcaster(Integer.valueOf(matcher.group(1)), matcher.group(2)));
      }
      return broadcasters;
    }

    private DatagramPacket sendGetBroadcastersRequest(DatagramSocket socket) throws UnknownHostException, IOException {
      String request = "GET BROADCASTERS\r\n\r\n";
      byte[] data = request.getBytes();
      DatagramPacket packet = new DatagramPacket(data, data.length);
      packet.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
      packet.setPort(PORT);
      socket.send(packet);
      return packet;
    }
  }
}
