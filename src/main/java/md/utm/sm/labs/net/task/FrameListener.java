package md.utm.sm.labs.net.task;

import java.awt.image.BufferedImage;

public interface FrameListener {
  void consumeFrame(BufferedImage frame);
}
