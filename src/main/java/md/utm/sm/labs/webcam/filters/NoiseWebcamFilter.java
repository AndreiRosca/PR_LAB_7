package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.NoiseFilter;

public class NoiseWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		NoiseFilter filter = new NoiseFilter();
		filter.filter(source, destination);
	}
}
