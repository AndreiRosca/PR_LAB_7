package md.utm.sm.labs.webcam.detector;

import java.awt.image.BufferedImage;
import java.util.List;

import md.utm.sm.labs.webcam.FrameProcessor;

public abstract class FaceRecognizingFrameProcessor implements FrameProcessor {

	@Override
	public final BufferedImage process(BufferedImage frame) {
		for (FaceBounds faceBounds : detectFaces(frame))
			drawFaceFrame(frame, faceBounds);
		return frame;
	}

	protected abstract List<FaceBounds> detectFaces(BufferedImage frame);

	protected abstract void drawFaceFrame(BufferedImage frame, FaceBounds f);
}
