package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.SphereFilter;

public class SphereWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		SphereFilter filter = new SphereFilter();
		filter.filter(source, destination);
	}
}
