package md.utm.sm.labs.webcam.motion;

import java.util.function.Consumer;

public abstract class MotionDetector {
	protected final Consumer<Boolean> motionConsumer;
	private volatile boolean stopRequested;
	
	public MotionDetector(Consumer<Boolean> motionConsumer) {
		this.motionConsumer = motionConsumer;
	}

	public void startDetection() {
		stopRequested = false;
		startDaemonThread(() -> detect());
	}
	
	private void startDaemonThread(Runnable r) {
		Thread t = new Thread(r);
		t.setDaemon(true);
		t.start();
	}
	
	public void stopDetection() {
		stopRequested = true;
	}

	private void detect() {
		startDetector();
		while (!stopRequested) {
			processMotionStatus(getMotionStatus());
			postProcessMotion();
		}
	}

	private void processMotionStatus(boolean isMotion) {
		if (isMotion)
			motionConsumer.accept(true);
		else
			motionConsumer.accept(false);
	}
	
	protected abstract void startDetector();
	
	protected abstract boolean getMotionStatus();
	
	protected abstract void postProcessMotion();
}
