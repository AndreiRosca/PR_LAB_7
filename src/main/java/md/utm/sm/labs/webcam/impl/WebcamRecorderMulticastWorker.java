package md.utm.sm.labs.webcam.impl;

import java.awt.image.BufferedImage;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import md.utm.sm.labs.mediator.VideoMediator;
import md.utm.sm.labs.net.task.FramePublisher;
import md.utm.sm.labs.net.task.StreamingGroup;
import md.utm.sm.labs.webcam.WebcamObserver;
import md.utm.sm.labs.webcam.WebcamRecorder;

public class WebcamRecorderMulticastWorker implements Runnable, WebcamObserver  {

  private ExecutorService service = Executors.newFixedThreadPool(10);
  private WebcamRecorder recorder;
  private VideoMediator mediator;
  private StreamingGroup group;
  
  public WebcamRecorderMulticastWorker(StreamingGroup g) {
    this.group = g;
  }
  
  public VideoMediator getMediator() {
    return mediator;
  }

  public void startRecording(VideoMediator mediator ) {
    this.mediator = mediator;
    recorder = new WebcamCaptureWebcamRecorder();
    getMediator().setWebcamRecorder(recorder);
    Thread t = new Thread(this);
    t.setDaemon(true);
    t.start();
  }

  @Override
  public void run() {
    WebcamRecorder recorder = getMediator().getWebcamRecorder();
    recorder.registerObserver(this);
    recorder.startRecording();
  }
  
  @Override
  public void consumeFrame(BufferedImage frame) {
    getMediator().setWebcamFrame(frame);
    service.submit(new FramePublisher(frame, group.getPort(), "230.1.1.1"));
  }

}
