package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.InvertFilter;

public class InvertWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		InvertFilter filter = new InvertFilter();
		filter.filter(source, destination);
	}
}
