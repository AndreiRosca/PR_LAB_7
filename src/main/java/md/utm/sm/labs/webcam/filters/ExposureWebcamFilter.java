package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.ExposureFilter;

public class ExposureWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		ExposureFilter filter = new ExposureFilter();
		filter.filter(source, destination);
	}
}
