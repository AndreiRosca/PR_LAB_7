package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.GaussianFilter;

public class GaussianWebcamFilter extends BaseWebcamFilter {
	
	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		GaussianFilter filter = new GaussianFilter(10);
		filter.filter(source, destination);
	}
}
