package md.utm.sm.labs.webcam.impl;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;

import md.utm.sm.labs.webcam.WebcamRecorder;

public class WebcamCaptureWebcamRecorder extends WebcamRecorder {

	private Webcam webcam;
	private long recordingStartTime;

	public WebcamCaptureWebcamRecorder() {
	}

	@Override
	protected void setUpCamera() {
		try {
			Dimension size = WebcamResolution.QVGA.getSize();
			webcam = Webcam.getDefault();
			webcam.setViewSize(size);
			webcam.open(true);
			recordingStartTime = System.currentTimeMillis();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void tearDownCamera() {
		webcam.close();
	}

	@Override
	protected BufferedImage getNextFrame() {
		return webcam.getImage();
	}

	@Override
	protected void writeFrame(BufferedImage image, boolean isFirstFrame) {
		IConverter converter = ConverterFactory.createConverter(image, IPixelFormat.Type.YUV420P);
		IVideoPicture frame = converter.toPicture(image, 
				(System.currentTimeMillis() - recordingStartTime) * 1000);
		frame.setKeyFrame(isFirstFrame);
		frame.setQuality(0);
		makeFpsDelay();
	}
	
	private void makeFpsDelay() {
		try {
			// 10 FPS
			Thread.sleep(100);
		} catch (InterruptedException e) {
			
		}
	}

	@Override
	protected BufferedImage postProcessFrame(BufferedImage frame) {
		return ConverterFactory.convertToType(frame, BufferedImage.TYPE_3BYTE_BGR);
	}
}
