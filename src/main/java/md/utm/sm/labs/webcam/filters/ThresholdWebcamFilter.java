package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.ThresholdFilter;

public class ThresholdWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		ThresholdFilter filter = new ThresholdFilter();
		filter.filter(source, destination);
	}
}
