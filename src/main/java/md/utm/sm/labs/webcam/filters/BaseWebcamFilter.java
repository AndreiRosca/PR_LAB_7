package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import md.utm.sm.labs.webcam.FrameProcessor;

public abstract class BaseWebcamFilter implements FrameProcessor {
	
	@Override
	public BufferedImage process(BufferedImage frame) {
		BufferedImage filteredImage = new BufferedImage(frame.getWidth(), 
				frame.getHeight(), BufferedImage.TYPE_INT_RGB);
		filter(frame, filteredImage);
		return filteredImage;
	}
	
	protected abstract void filter(BufferedImage source, BufferedImage destination);
}
