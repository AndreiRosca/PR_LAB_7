package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.CrystallizeFilter;

public class CristalizeWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		CrystallizeFilter filter = new CrystallizeFilter();
		filter.filter(source, destination);
	}
}
