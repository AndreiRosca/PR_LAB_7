package md.utm.sm.labs.webcam;

import java.awt.image.BufferedImage;

public interface FrameProcessor {
	BufferedImage process(BufferedImage frame);
}
