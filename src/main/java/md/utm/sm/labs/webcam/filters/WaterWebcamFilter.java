package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.WaterFilter;

public class WaterWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		WaterFilter filter = new WaterFilter();
		filter.filter(source, destination);
	}
}
