package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.LightFilter;

public class LightWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		LightFilter filter = new LightFilter();
		filter.filter(source, destination);
	}
}
