package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.FBMFilter;

public class FbmWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		FBMFilter filter = new FBMFilter();
		filter.filter(source, destination);
	}
}
