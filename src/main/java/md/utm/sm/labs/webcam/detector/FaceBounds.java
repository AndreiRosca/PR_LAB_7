package md.utm.sm.labs.webcam.detector;

public class FaceBounds {
	private double x;
	private double y;
	private double width;
	private double height;

	private FaceBounds() {
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}
	
	public static FaceBoundsBuilder newBuilder() {
		return new FaceBoundsBuilder();
	}

	public static class FaceBoundsBuilder {
		private FaceBounds faceBounds = new FaceBounds();
		
		public FaceBoundsBuilder setX(double x) {
			faceBounds.x = x;
			return this;
		}
		
		public FaceBoundsBuilder setY(double y) {
			faceBounds.y = y;
			return this;
		}
		
		public FaceBoundsBuilder setWidth(double width) {
			faceBounds.width = width;
			return this;
		}
		
		public FaceBoundsBuilder setHeight(double height) {
			faceBounds.height = height;
			return this;
		}
		
		public FaceBounds build() {
			return faceBounds;
		}
	}
}
