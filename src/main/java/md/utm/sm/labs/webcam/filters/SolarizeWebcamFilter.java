package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.SolarizeFilter;

public class SolarizeWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		SolarizeFilter filter = new SolarizeFilter();
		filter.filter(source, destination);
	}
}
