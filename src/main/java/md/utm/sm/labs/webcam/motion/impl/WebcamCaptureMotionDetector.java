package md.utm.sm.labs.webcam.motion.impl;

import java.util.function.Consumer;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionDetector;

import md.utm.sm.labs.webcam.motion.MotionDetector;

public class WebcamCaptureMotionDetector extends MotionDetector {

	private final Webcam webcam;
	private WebcamMotionDetector detector;
	private final int MOTION_CHECK_INTERVAL_IN_MILLISECONDS = 100;

	public WebcamCaptureMotionDetector(Consumer<Boolean> motionConsumer) {
		super(motionConsumer);
		this.webcam = Webcam.getDefault();
		setUpDetector();
	}

	private void setUpDetector() {
		int threshold = 25;
		int inertia = 10;
		detector = new WebcamMotionDetector(webcam, threshold, inertia);
	}

	@Override
	protected void startDetector() {
		detector.setInterval(MOTION_CHECK_INTERVAL_IN_MILLISECONDS);
		detector.start();
	}

	@Override
	protected boolean getMotionStatus() {
		return detector.isMotion();
	}

	@Override
	protected void postProcessMotion() {
		try {
			Thread.sleep(2 * MOTION_CHECK_INTERVAL_IN_MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
