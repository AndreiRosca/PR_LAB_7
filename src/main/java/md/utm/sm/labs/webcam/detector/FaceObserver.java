package md.utm.sm.labs.webcam.detector;

import java.util.List;

import org.openimaj.image.processing.face.detection.DetectedFace;

public interface FaceObserver {
	void consumeFaces(List<DetectedFace> faces);
}
