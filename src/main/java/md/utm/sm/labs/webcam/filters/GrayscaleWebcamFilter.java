package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.GrayscaleFilter;

public class GrayscaleWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		GrayscaleFilter filter = new GrayscaleFilter();
		filter.filter(source, destination);
	}
}
