package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class NoneWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		int width = source.getWidth();
		int height = source.getHeight();
		int type = source.getType();
		WritableRaster srcRaster = source.getRaster();
		WritableRaster dstRaster = destination.getRaster();
		int[] inPixels = new int[width];
		for (int y = 0; y < height; y++) {
			if (type == BufferedImage.TYPE_INT_ARGB) {
				srcRaster.getDataElements(0, y, width, 1, inPixels);
				dstRaster.setDataElements(0, y, width, 1, inPixels);
			} else {
				source.getRGB(0, y, width, 1, inPixels, 0, width);
				destination.setRGB(0, y, width, 1, inPixels, 0, width);
			}
		}
	}
}
