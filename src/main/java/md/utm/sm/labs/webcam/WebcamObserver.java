package md.utm.sm.labs.webcam;

import java.awt.image.BufferedImage;

public interface WebcamObserver {
	void consumeFrame(BufferedImage frame);
}
