package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.GlowFilter;

public class GlowWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		GlowFilter filter = new GlowFilter();
		filter.filter(source, destination);
	}
}
