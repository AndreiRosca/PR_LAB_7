package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.SharpenFilter;

public class SharperWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		SharpenFilter filter = new SharpenFilter();
		filter.filter(source, destination);
	}
}
