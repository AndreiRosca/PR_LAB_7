package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.KaleidoscopeFilter;

public class KaleidoscopeWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		KaleidoscopeFilter filter = new KaleidoscopeFilter();
		filter.filter(source, destination);
	}
}
