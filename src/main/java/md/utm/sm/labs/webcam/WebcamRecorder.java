package md.utm.sm.labs.webcam;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

public abstract class WebcamRecorder implements FrameProcessor {

  private List<WebcamObserver> observers = new CopyOnWriteArrayList<>();
  private volatile boolean stopRequested;
  private final AtomicReference<FrameProcessor> processor = new AtomicReference<>(this);

  public WebcamRecorder() {
  }

  public void registerObserver(WebcamObserver observer) {
    observers.add(observer);
  }

  public void setFrameProcessor(FrameProcessor processor) {
    this.processor.set(processor);
  }

  public void stopRecording() {
    stopRequested = true;
  }

  public void startRecording() {
    stopRequested = false;
    setUpCamera();
    for (int i = 0; !stopRequested; ++i) {
      BufferedImage processedFrame = processor.get()
          .process(getNextFrame());
      BufferedImage image = postProcessFrame(processedFrame);
      publishFrameToObservers(image);
      writeFrame(image, i == 0);
    }
    tearDownCamera();
  }

  protected abstract void setUpCamera();

  protected abstract void tearDownCamera();

  protected abstract BufferedImage getNextFrame();

  protected abstract void writeFrame(BufferedImage frame, boolean isFirstFrame);

  protected abstract BufferedImage postProcessFrame(BufferedImage frame);

  private void publishFrameToObservers(BufferedImage frame) {
    for (WebcamObserver o : observers)
      o.consumeFrame(frame);
  }

  @Override
  public BufferedImage process(BufferedImage frame) {
    return frame;
  }

  public static final WebcamRecorder NULL_RECORDER = new WebcamRecorder() {
    public void registerObserver(WebcamObserver observer) {
    }

    public void setFrameProcessor(FrameProcessor processor) {
    }

    public void stopRecording() {
    }

    public void startRecording() {
    }

    @Override
    protected void setUpCamera() {
    }

    @Override
    protected void tearDownCamera() {
    }

    @Override
    protected BufferedImage getNextFrame() {
      return null;
    }

    @Override
    protected void writeFrame(BufferedImage frame, boolean isFirstFrame) {
    }

    @Override
    protected BufferedImage postProcessFrame(BufferedImage frame) {
      return null;
    }
  };
}
