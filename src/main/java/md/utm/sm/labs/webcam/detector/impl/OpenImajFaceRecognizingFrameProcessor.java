package md.utm.sm.labs.webcam.detector.impl;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.math.geometry.shape.Rectangle;

import md.utm.sm.labs.webcam.detector.FaceBounds;
import md.utm.sm.labs.webcam.detector.FaceRecognizingFrameProcessor;

public class OpenImajFaceRecognizingFrameProcessor extends FaceRecognizingFrameProcessor {
	
	private final HaarCascadeDetector detector = new HaarCascadeDetector();
	
	protected void drawFaceFrame(BufferedImage frame, FaceBounds bounds) {
		int dx = (int) (0.1 * bounds.getWidth());
		int dy = (int) (0.2 * bounds.getHeight());
		int x = (int) bounds.getX() - dx;
		int y = (int) bounds.getY() - dy;
		int w = (int) bounds.getWidth() + 2 * dx;
		int h = (int) bounds.getHeight() + dy;
		Graphics2D g = frame.createGraphics();
		float thickness = 2f;
		Stroke oldStroke = g.getStroke();
		g.setStroke(new BasicStroke(thickness));
		g.drawRect(x, y, w, h);
		g.setStroke(oldStroke);
	}

	@Override
	protected List<FaceBounds> detectFaces(BufferedImage frame) {
		List<DetectedFace> faces = detector.detectFaces(ImageUtilities.createFImage(frame));
		List<FaceBounds> detectedFaces = new ArrayList<>();
		for (DetectedFace face : faces)
			detectedFaces.add(getFaceBoundsFrom(face.getBounds()));
		return detectedFaces;
	}

	private FaceBounds getFaceBoundsFrom(Rectangle bounds) {
		return FaceBounds.newBuilder()
				.setX(bounds.x)
				.setY(bounds.y)
				.setWidth(bounds.width)
				.setHeight(bounds.height)
				.build();
	}
}
