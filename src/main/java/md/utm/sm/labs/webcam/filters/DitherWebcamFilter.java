package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.DitherFilter;

public class DitherWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		DitherFilter filter = new DitherFilter();
		filter.filter(source, destination);
	}
}
