package md.utm.sm.labs.webcam.filters;

import java.awt.image.BufferedImage;

import com.jhlabs.image.GammaFilter;

public class GammaWebcamFilter extends BaseWebcamFilter {

	@Override
	protected void filter(BufferedImage source, BufferedImage destination) {
		GammaFilter filter = new GammaFilter();
		filter.filter(source, destination);
	}
}
