package md.utm.sm.labs.mediator;

import java.awt.image.BufferedImage;

import md.utm.sm.labs.webcam.WebcamRecorder;

public interface VideoMediator {
	void setWebcamFrame(BufferedImage frame);
	void clearWebcamImageView();
	void setWebcamRecorder(WebcamRecorder recorder);
	void onMotionDetected();
	void onMotionNotDetected();
	WebcamRecorder getWebcamRecorder();
	void enableMotionDetectionStatus();
	void registerFilterChangeListener(FilterChangeListener listener);
}
