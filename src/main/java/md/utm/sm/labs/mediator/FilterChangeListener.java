package md.utm.sm.labs.mediator;

import md.utm.sm.labs.webcam.FrameProcessor;

public interface FilterChangeListener {
	void onFilterChange(FrameProcessor filter);
}
