package md.utm.sm.labs.video;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;

import md.utm.sm.labs.webcam.FrameProcessor;

public class VideoFilterTransformer {
	private final File sourceVideo;
	private final File destinationVideo;
	private final List<FilterTransformerObserver> observers = new CopyOnWriteArrayList<>();
	private final AtomicReference<FrameProcessor> currentFilter = new AtomicReference<>();
	
	public VideoFilterTransformer(File sourceVideo, File destinationVideo, FrameProcessor filter) {
		this.sourceVideo = sourceVideo;
		this.destinationVideo = destinationVideo;
		currentFilter.set(filter);
	}
	
	public void transform() {
		new Thread(() -> {
			new FilterHelper().applyFilter();
		}).start();
			
	}
	
	public void registerObserver(FilterTransformerObserver o) {
		observers.add(o);
	}
	
	public void changeFilter(FrameProcessor filter) {
		currentFilter.set(filter);
	}
	
	private void publishFrameToObservers(BufferedImage frame) {
		for (FilterTransformerObserver o : observers) {
			o.consumeFrame(frame);
		}
	}
	
	private class FilterHelper {
		private IMediaWriter mWriter = ToolFactory.makeWriter(destinationVideo.toString());
		private IStreamCoder coderVideo;
		private int width;
		private int height;
		private IPacket packet = IPacket.make();
		private IContainer containerVideo = IContainer.make();

		public void applyFilter() {
			checkIfContainersAreOpened();
			coderVideo = containerVideo.getStream(0).getStreamCoder();
			checkIfCoderIsOpen(coderVideo);
			width = coderVideo.getWidth();
			height = coderVideo.getHeight();
			mWriter.addVideoStream(0, 0, width, height);
			while (containerVideo.readNextPacket(packet) >= 0) {
				convertVideoPacket();
			}
			closeStreams();
		}

		private void checkIfCoderIsOpen(IStreamCoder coder) {
			if (coder.open(null, null) < 0) {
				throw new RuntimeException("Cant open the coder");
			}
		}

		private void closeStreams() {
			coderVideo.close();
			containerVideo.close();
			mWriter.close();
		}

		private void convertVideoPacket() {
			IVideoPicture picture = IVideoPicture.make(coderVideo.getPixelType(), width, height);
			coderVideo.decodeVideo(picture, packet, 0);
			if (picture.isComplete()) {
				IConverter converter = ConverterFactory.createConverter(ConverterFactory.XUGGLER_BGR_24, picture);
				long timeStamp = picture.getTimeStamp();
				BufferedImage image = converter.toImage(picture);
				BufferedImage filteredFrame = currentFilter.get().process(image);
				filteredFrame =  ConverterFactory.convertToType(filteredFrame, BufferedImage.TYPE_3BYTE_BGR);
				publishFrameToObservers(filteredFrame);
				IVideoPicture filteredPicture = converter.toPicture(filteredFrame, timeStamp);
				mWriter.encodeVideo(0, filteredPicture);
			}
		}

		private void checkIfContainersAreOpened() {
			if (containerVideo.open(sourceVideo.toString(), IContainer.Type.READ, null) < 0) {
				throw new IllegalArgumentException("Cant find " + sourceVideo);
			}
		}
	}
}
