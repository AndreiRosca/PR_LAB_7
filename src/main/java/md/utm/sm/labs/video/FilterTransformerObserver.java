package md.utm.sm.labs.video;

import java.awt.image.BufferedImage;

public interface FilterTransformerObserver {
	void consumeFrame(BufferedImage frame);
}
