package md.utm.sm.labs.video;

import java.awt.image.BufferedImage;
import java.io.File;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.IAudioSamples;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.video.IConverter;

public class VideoAudioMerger {
	private File sourceVideo;
	private File sourceAudio;
	private File destinationVideo;

	private VideoAudioMerger() {
	}

	public void merge() {
		new MergeHelper().merge();
	}

	private class MergeHelper {
		private IMediaWriter mWriter = ToolFactory.makeWriter(destinationVideo.toString());
		private IStreamCoder coderVideo;
		private IStreamCoder coderAudio;
		private int width;
		private int height;
		private IPacket packet = IPacket.make();
		private IContainer containerVideo = IContainer.make();
		private IContainer containerAudio = IContainer.make();

		public void merge() {
			checkIfContainersAreOpened();
			coderVideo = containerVideo.getStream(0).getStreamCoder();
			checkIfCoderIsOpen(coderVideo);
			width = coderVideo.getWidth();
			height = coderVideo.getHeight();
			coderAudio = containerAudio.getStream(0).getStreamCoder();
			checkIfCoderIsOpen(coderAudio);
			mWriter.addAudioStream(1, 0, coderAudio.getChannels(), coderAudio.getSampleRate());
			mWriter.addVideoStream(0, 0, width, height);
			while (containerVideo.readNextPacket(packet) >= 0) {
				convertVideoPacket();
				convertAudioPacket();
			}
			appendRemainingAudioIfNeeded();
			closeStreams();
		}

		private void checkIfCoderIsOpen(IStreamCoder coder) {
			if (coder.open(null, null) < 0) {
				throw new RuntimeException("Cant open the coder");
			}
		}

		private void closeStreams() {
			coderAudio.close();
			coderVideo.close();
			containerAudio.close();
			containerVideo.close();
			mWriter.close();
		}

		private void convertAudioPacket() {
			containerAudio.readNextPacket(packet);
			IAudioSamples samples = IAudioSamples.make(512, coderAudio.getChannels(), IAudioSamples.Format.FMT_S32);
			coderAudio.decodeAudio(samples, packet, 0);
			if (samples.isComplete()) {
				mWriter.encodeAudio(1, samples);
			}
		}

		private void convertVideoPacket() {
			IVideoPicture picture = IVideoPicture.make(coderVideo.getPixelType(), width, height);
			coderVideo.decodeVideo(picture, packet, 0);
			if (picture.isComplete()) {
				IConverter converter = ConverterFactory.createConverter(ConverterFactory.XUGGLER_BGR_24, picture);
				long timeStamp = picture.getTimeStamp();
				BufferedImage image = converter.toImage(picture);
				//
				IVideoPicture filteredPicture = converter.toPicture(image, timeStamp);
				mWriter.encodeVideo(0, filteredPicture);
			}
		}

		private void appendRemainingAudioIfNeeded() {
			while (containerAudio.readNextPacket(packet) >= 0) {
				IAudioSamples samples = IAudioSamples.make(512, coderAudio.getChannels(), IAudioSamples.Format.FMT_S32);
				coderAudio.decodeAudio(samples, packet, 0);
				if (samples.isComplete()) {
					mWriter.encodeAudio(1, samples);
				}
			}
		}

		private void checkIfContainersAreOpened() {
			if (containerVideo.open(sourceVideo.toString(), IContainer.Type.READ, null) < 0) {
				throw new IllegalArgumentException("Cant find " + sourceVideo);
			}
			if (containerAudio.open(sourceAudio.toString(), IContainer.Type.READ, null) < 0) {
				throw new IllegalArgumentException("Cant find " + sourceAudio.toString());
			}
		}
	}

	public static VideoAudioMergerBuilder newBuilder() {
		return new VideoAudioMergerBuilder();
	}

	public static class VideoAudioMergerBuilder {
		private VideoAudioMerger merger = new VideoAudioMerger();

		public VideoAudioMergerBuilder setSourceVideo(File sourceVideo) {
			merger.sourceVideo = sourceVideo;
			return this;
		}

		public VideoAudioMergerBuilder setSourceAudio(File sourceAudio) {
			merger.sourceAudio = sourceAudio;
			return this;
		}

		public VideoAudioMergerBuilder setDestinationVideo(File destinationVideo) {
			merger.destinationVideo = destinationVideo;
			return this;
		}

		public VideoAudioMerger build() {
			return merger;
		}
	}
}
