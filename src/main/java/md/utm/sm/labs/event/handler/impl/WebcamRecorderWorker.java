package md.utm.sm.labs.event.handler.impl;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.sm.labs.mediator.VideoMediator;
import md.utm.sm.labs.net.task.FramePublisher;
import md.utm.sm.labs.webcam.WebcamObserver;
import md.utm.sm.labs.webcam.WebcamRecorder;
import md.utm.sm.labs.webcam.impl.WebcamCaptureWebcamRecorder;

public class WebcamRecorderWorker implements Runnable, WebcamObserver {
	
  private static final int PORT = 6000;
  private static final int MAX_PACKET_SIZE = 12_000;
  private static final String BROADCAST_ADDRESS = "192.168.1.255";
  private static final Pattern HEADER_PATTERN = Pattern.compile("(.+): (.+)");
  
  private int port = 9898;
  
  private ExecutorService service = Executors.newFixedThreadPool(10);
  private WebcamRecorder recorder;
  private VideoMediator mediator;
  
	public WebcamRecorderWorker() {
	}
	
	public VideoMediator getMediator() {
	  return mediator;
	}

	public void startRecording(VideoMediator mediator ) {
	  this.mediator = mediator;
		recorder = new WebcamCaptureWebcamRecorder();
		getMediator().setWebcamRecorder(recorder);
		Thread t = new Thread(this);
		t.setDaemon(true);
		t.start();
	}

	@Override
	public void run() {
	  port = getAvailablePort();
	  System.out.println("Received port: " + port);
		WebcamRecorder recorder = getMediator().getWebcamRecorder();
		recorder.registerObserver(this);
		recorder.startRecording();
	}

  private int getAvailablePort() {
    DatagramSocket socket = null;
    try {
      socket = new DatagramSocket();
      DatagramPacket portRequest = sendGetPortRequest(socket);
      byte[] buffer = new byte[MAX_PACKET_SIZE];
      DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
      packet.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
      packet.setPort(portRequest.getPort());
      do {
        socket.receive(packet);
        if (new String(packet.getData()).contains("RESPONSE"))
          break;
      } while (true);
      return extractPortFromResponse(packet);
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      if (socket != null)
        socket.close();
    } 
  }
	
  private int extractPortFromResponse(DatagramPacket packet) {
    byte[] data = packet.getData();
    System.out.println("Received data: " + new String(data));
    try (BufferedReader reader = new BufferedReader(new StringReader(new String(data)))) {
      if (reader.readLine().startsWith("GET PORT RESPONSE")) {
        Map<String, String> headers = readHeaders(reader);
        System.out.println(headers);
        return Integer.valueOf(headers.get("Available-Port"));
      }
      return 0;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  
  public static Map<String, String> readHeaders(BufferedReader reader) throws IOException {
    Map<String, String> headers = new HashMap<>();
    String header = "";
    do {
      header = reader.readLine();
      if (header == null)
        break;
      Matcher m = HEADER_PATTERN.matcher(header);
      if (m.find())
        headers.put(m.group(1), m.group(2));
    } while (!"".equals(header));
    return headers;
  }

  private DatagramPacket sendGetPortRequest(DatagramSocket socket) throws UnknownHostException, IOException {
    String request = "GET PORT\r\nBroadcaster-Name: Mike\r\n\r\n";
    byte[] data = request.getBytes();
    DatagramPacket packet = new DatagramPacket(data, data.length);
    packet.setPort(PORT);
    packet.setAddress(InetAddress.getByName(BROADCAST_ADDRESS));
    socket.send(packet);
    return packet;
  }

	@Override
	public void consumeFrame(BufferedImage frame) {
		getMediator().setWebcamFrame(frame);
		service.submit(new FramePublisher(frame, port));
	}
}
