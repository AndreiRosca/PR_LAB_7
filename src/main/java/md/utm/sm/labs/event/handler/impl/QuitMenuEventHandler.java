package md.utm.sm.labs.event.handler.impl;

import javafx.application.Platform;
import md.utm.pr.labs.event.handler.BaseMenuEventHandler;

public class QuitMenuEventHandler extends BaseMenuEventHandler {

	public QuitMenuEventHandler() {
		super("quitMenuItemId");
	}

	@Override
	public void handle() {
		Platform.exit();
	}
}
