package md.utm.sm.labs.event.handler.impl;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import md.utm.pr.labs.event.handler.BaseMenuEventHandler;

public class AboutMenuEventHandler extends BaseMenuEventHandler {
	private static final String EVENT_ID = "aboutMenuItemId";

	public AboutMenuEventHandler() {
		super(EVENT_ID);
	}

	@Override
	public void handle() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("About");
		alert.setHeaderText("Written by Andrei Roșca, aka \"Cel mai bun programist\"");
		alert.setContentText("UTM 2017; License: GNU General Public License Version 2");
		alert.showAndWait();
	}
}
