package md.utm.sm.labs.event.handler.impl;

import md.utm.pr.labs.event.handler.BaseMenuEventHandler;

public class StopWatchingBroadcastEventHandler extends BaseMenuEventHandler implements Runnable {


  public StopWatchingBroadcastEventHandler() {
    super("stopWatchingBroadcastMenuItemId");
  }

  @Override
  public void handle() {
    Thread t = new Thread(this);
    t.setDaemon(true);
    t.start();
  }

  @Override
  public void run() {
    
  }
}
