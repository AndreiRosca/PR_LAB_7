package md.utm.sm.labs.event.handler.impl;

import md.utm.pr.labs.event.handler.BaseMenuEventHandler;
import md.utm.sm.labs.webcam.WebcamRecorder;
import md.utm.sm.labs.webcam.detector.FaceRecognizingFrameProcessor;
import md.utm.sm.labs.webcam.detector.impl.OpenImajFaceRecognizingFrameProcessor;

public class FaceRecognitionEventHandler extends BaseMenuEventHandler {

	public FaceRecognitionEventHandler() {
		super("faceRecognitionMenuItemId");
	}

	@Override
	public void handle() {
		FaceRecognizingFrameProcessor processor = new OpenImajFaceRecognizingFrameProcessor();
		WebcamRecorder recorder = getMediator().getWebcamRecorder();
		recorder.setFrameProcessor(processor);
	}
}
