package md.utm.sm.labs.event.handler.impl;

import java.awt.image.BufferedImage;

import md.utm.pr.labs.event.handler.BaseMenuEventHandler;
import md.utm.sm.labs.net.task.Broadcaster;
import md.utm.sm.labs.net.task.BroadcastersFetcher;
import md.utm.sm.labs.net.task.FrameListener;
import md.utm.sm.labs.net.task.FrameSubscriber;

public class StartWatchingBroadcastEventHandler extends BaseMenuEventHandler implements FrameListener {

  public StartWatchingBroadcastEventHandler() {
    super("startWatchingBroadcastMenuItemId");
  }

  @Override
  public void handle() {
    BroadcastersFetcher fetcher = new BroadcastersFetcher();
    fetcher.getBroadcasters().thenAccept(l -> {
      System.out.println(l);
      Broadcaster first = l.get(0);
      FrameSubscriber subscriber = new FrameSubscriber(first.getPort());
      subscriber.registerListener(this);
      subscriber.start();
    });
  }

  @Override
  public void consumeFrame(BufferedImage frame) {
    getMediator().setWebcamFrame(frame);
  }
}
