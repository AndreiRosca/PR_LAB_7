package md.utm.sm.labs.event.handler.impl;

import md.utm.pr.labs.event.handler.BaseMenuEventHandler;

public class StopWebcamRecordingEventHandler extends BaseMenuEventHandler {

	public StopWebcamRecordingEventHandler() {
		super("stopWebcamRecordingMenuItemId");
	}

	@Override
	public void handle() {
		System.out.println("stop");
		getMediator().getWebcamRecorder().stopRecording();
		getMediator().clearWebcamImageView();
	}
}
